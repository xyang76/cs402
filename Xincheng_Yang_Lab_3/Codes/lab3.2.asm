.data 0x10010000
var1: .word 0x2 # var1 is a word (32 bit) with the initial value 0x2
var2: .word 0x0 # var2 is a word (32 bit) with the initial value 0x0
var3: .word -2016 # var3 is a word (32 bit) with the initial value -2016
.text
.globl main
main: addu $s0, $ra, $0
lw $s1, var1		#lw $s1, var1
lw $s2, var2		#lw $s2, var2
lw $s3, var3		#lw $s3, var3
slt $t0, $s1, $s2	#if(var1<var2)
bgtz $t0, else		#jump else
move $s1, $s3		#var1=var3
move $s2, $s3		#var2=var3
j end			#jump end
else: move $t0, $s1	#temp=var1
move $s1, $s2		#var1=var2
move $s2, $t0		#var2=temp
end: sw $s1, var1	#save var1
sw $s2, var2		#save var2
addu $ra, $0, $s0 # return address back in $31
jr $ra # return from main