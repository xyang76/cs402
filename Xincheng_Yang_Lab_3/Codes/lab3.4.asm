.data 0x10010000
my_array: .space 40 #word(4)*10=40
initial_value: .word 2 #first digit of my SSN
.text
.globl main
main: addu $s0, $ra, $0
lw $t1, initial_value	#set j=2(j=initial_value)
move $t0, $zero		#set i=0
li $t2, 10		#set max=10
la $t3, my_array	
loop: ble $t2, $t0, exit #exit when i>=10
sll $t4, $t0, 2		#set t4=4*i.
add $t5, $t3, $t4	#get address of my_array[i]
sw $t1, 0($t5)		#my_array[i]=j;
addi $t1, $t1, 1	#j++
addi $t0, $t0, 1	#i++
j loop
exit: addu $ra, $0, $s0 # return address back in $31
jr $ra # return from main