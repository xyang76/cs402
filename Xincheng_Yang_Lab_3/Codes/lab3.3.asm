.data 0x10010000
var1: .word 0x2 # var1 is a word (32 bit) with the initial value 0x2
.text
.globl main
main: addu $s0, $ra, $0
lw $a0, var1		#lw $s1, var1
li $a1, 100		#set a1=100
move $t0, $a0		#move t0=var1
loop: ble $a1, $t0, exit #exit when i>=100
addi $a0, $a0, 1	#var1 = var1+1
sw $a0, var1		#save var1
addi $t0, $t0, 1	#i++
j loop
exit: addu $ra, $0, $s0 # return address back in $31
jr $ra # return from main