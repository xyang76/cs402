.data 0x10010000
msgt0: .asciiz "Please enter the integer number of $t0: "
msgt1: .asciiz "Please enter an integer number of $t1: "
far: .asciiz "I'am far away"
near: .asciiz "I'am nearby"
.text
.globl main
main: addu $s0, $ra, $0
li $v0, 4 	# system call for print_str
la $a0, msgt0 	# address of msgt0
syscall
li $v0, 5 	# system call for read_int
syscall 	# the integer placed in $v0
move $t0, $v0 	# move the number in $t0
li $v0, 4 	# system call for print_str
la $a0, msgt1 	# address of msgt0
syscall
li $v0, 5 	# system call for read_int
syscall 	# the integer placed in $v0
move $t1, $v0 	# move the number in $t1
bne $t0, $t1, Near	# if t0!=t1, jump near
j Far
Near: la $a0, near	# load near.
li $v0, 4 	# system call for print_str
syscall
exit: addu $ra, $0, $s0 # return address back in $31
jr $ra # return from main
.text 0x0043ffe0
Far: 		
la $a0, far	# load far.
li $v0, 4 	# system call for print_str
syscall	
j exit