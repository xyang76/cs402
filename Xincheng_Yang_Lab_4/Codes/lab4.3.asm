      .data 0x10000000
msg1: .asciiz "Please enter an integer number: "
msg2: .asciiz "Please enter another integer number: "
msg3: .asciiz "The largest  number: "
.text
.globl main
main: add $sp, $sp, -4 # must save $ra since I’ll have a call
      sw $ra, 4($sp)      
      li $v0, 4 # system call for print_str
      la $a0, msg1 # address of string to print
      syscall # now get an integer from the user
      li $v0, 5 # system call for read_int
      syscall # the integer placed in $v0
      # do some computation here with the number
      addu $t0, $v0, $0 # move the number in $t0
      li $v0, 4 # system call for print_str
      la $a0, msg2 # address of string to print
      syscall
      # now get an integer from the user
      li $v0, 5 # system call for read_int
      syscall # the integer placed in $v0
      # do some computation here with the number
      addu $t1, $v0, $0 # move the number in $t0
      move $a0, $t0
      move $a1, $t1
      jal largest # call ‘test’ with no parameters
      nop # execute this after ‘test’ returns
      lw $ra, 4($sp) # restore the return address in $ra
      add $sp, $sp, 4
      jr $ra # return from main
      # The procedure ‘test’ does not call any other procedure. Therefore $ra
      # does not need to be saved. Since ‘test’ uses no registers there is
      # no need to save any registers.
largest: add $sp, $sp, -4
         sw $ra, 4($sp)
         move $t0, $a0
         move $t1, $a1
         slt $t2, $t0, $t1
         bgtz $t2, output1
         li $v0, 4 # system call for print_str
         la $a0, msg3 # address of string to print
         syscall
         li $v0, 1 # system call for print_int
         addu $a0, $t0, $0 # move number to print in $a0
         syscall
         lw $ra, 4($sp)
         add $sp, $sp, 4
         jr $ra # return from this procedure
output1: move $t3, $a1 
         li $v0, 4 # system call for print_str
         la $a0, msg3 # address of string to print
         syscall
         li $v0, 1 # system call for print_int
         addu $a0, $t3, $0 # move number to print in $a0
         syscall
         lw $ra, 4($sp)
         add $sp, $sp, 4
         jr $ra # return from this procedure
