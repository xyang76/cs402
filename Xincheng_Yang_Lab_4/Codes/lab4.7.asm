.data 0x10010000
msg1: .asciiz "Please enter the x of Ackermann's function A(x,y): "

msg2: .asciiz "Please enter the y of Ackermann's function A(x,y): "
msg3: .asciiz "The result is: "
.text 
.globl main
main: 
sub $sp, $sp, 4
sw $ra, 0($sp)

li $v0, 4 	# system call for print_str
      
la $a0, msg1	# address of string to print
      
syscall 	# now get an integer from the user
li $v0, 5 	# system call for read_int

syscall		# read int x
move $t0, $v0	# set $t0=x

li $v0, 4 	# system call for print_str
      
la $a0, msg1	# address of string to print
      
syscall 	# now get an integer from the user
li $v0, 5 	# system call for read_int

syscall		# read int y 
move $t1, $v0	# set $t1=y
jal Ackermann	# call Ackermann
nop

move $t2, $v0	# This block is Ackermann returned.
li $v0, 4 	# system call for print_str
      
la $a0, msg3	# address of string to print
      
syscall 	# now get an integer from the user
move $a0, $t2	# move the result
li $v0, 1	# system call for print_int
syscall		# print the result		
lw $ra, 0($sp)	# return main, pop address from stack
add $sp, $sp, 4	# return main
jr $ra		# return main

Ackermann: 
sub $sp, $sp, 4		# push address stack
sw $ra, 0($sp)		# push address stack
beq $t0, $0, branch1	# If x=0, goto Branch1
beq $t1, $0, branch2	# Else if y=0, goto Branch2
j branch3		# Else goto Branch3

branch1:		# When x=0
addi $v0, $t1, 1	# If x=0, then return y+1
j return

branch2: 		# When y=0
sub $t0, $t0, 1		# x=x-1
li $t1, 1		# y=1
jal Ackermann		# Call Ackermann A(x-1, 1)
nop
j return

branch3: 
sub $sp, $sp, 4		# Sub stack to save x;
sw $t0, 0($sp)		# Save x to stack;
sub $t1, $t1, 1		# y=y-1
jal Ackermann		# Call Ackermann A(x, y-1)
nop
lw $t0, 0($sp)		# Load x from stack
addi $sp, $sp, 4	# Add stack, pop x;
sub $t0, $t0, 1		# x=x-1
move $t1, $v0		# y=A(x,y-1)
jal Ackermann		# Call Ackermann A(x-1, A(x,y-1))
nop
j return

return: lw $ra, 0($sp) 	# pop address stack
addi $sp, $sp, 4	# push address stack
jr $ra			# return Ackermann