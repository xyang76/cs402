#include <stdio.h>
int Ackermann(int x, int y);

int main(int argc, char **argv)
{
    char* msg1 = "Please enter the x of Ackermann's function A(x,y): ";
    char* msg2 = "Please enter the y of Ackermann's function A(x,y): ";
    char* msg3 = "The result is: ";
    int x, y;
    printf("%s", msg1);
    scanf("%d", &x);
    printf("%s", msg2);
    scanf("%d", &y);
    
    printf("%s", msg3);
    printf("%d", Ackermann(x,y));
}

int Ackermann(int x, int y){
    if(x==0){
        return y+1;
    } else if(y==0){
        return Ackermann(x-1, 1);
    } else{
        return Ackermann(x-1, Ackermann(x, y-1));
    }
}
