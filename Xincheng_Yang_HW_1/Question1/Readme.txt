------------Comment---------------------
The 2 diagrams shows the memory addresses when program running, some of the addresses are frequently used, perhaps they are some function blocks. and the address always add 4 if no program jump.   

-------------Gnuplot command-------------
set title "Tex histogram(too much address so that can not see any address in xtics)"
set ylabel "Occurence"  
set xlabel "Address"  
set style fill solid border 3
plot "texd.data" using 2:xtic(1) with histogram ls 3

set title "Spice histogram(too much address so that can not see any address in xtics)"
set ylabel "Occurence"  
set xlabel "Address"  
set style fill solid border 3
plot "spiced.data" using 2:xtic(1) with histogram ls 3
