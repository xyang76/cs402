import java.lang.reflect.Type;
import java.util.Random;
import java.util.Scanner;

/**
 * @author Xincheng Yang(xyang76@hawk.iit.edu)
 *
 */
public class HW1 {
	public static void main(String[] args) throws Exception {
		new HW1().run();
	}

	public void run() throws Exception {
		final double NANO_FACTOR = 1000000000.0;
		long start, end;
		Type type = null;

		Scanner sc = new Scanner(System.in);
		System.out.println("Please input data type[accept string or number: int(0) or double(1)]:");
		String datatype = sc.nextLine().trim().toLowerCase();
		if (datatype.equals("0") || datatype.equals("int")) {
			type = Integer.TYPE;
		} else if (datatype.equals("1") || datatype.equals("double")) {
			type = Double.TYPE;
		} else {
			throw new Exception("wrong input.");
		}

		System.out.println("Please input the rows and columns of 2 item[eg: 100,200,200,300]");
		String[] recdata = sc.nextLine().trim().split(",");
		if (recdata.length != 4) {
			throw new Exception("wrong input.");
		}

		double totalTime = 0;
		for(int i=0; i<5; i++){
			start = System.nanoTime();
			
			Rectangular item1 = rectangularGenerator(type, Integer.parseInt(recdata[0].trim()),
					Integer.parseInt(recdata[1].trim()));
			Rectangular item2 = rectangularGenerator(type, Integer.parseInt(recdata[2].trim()),
					Integer.parseInt(recdata[3].trim()));

			Rectangular result = rectangularMultiple(type, item1, item2);
			
			end = System.nanoTime();
			double timecost = (end - start) / NANO_FACTOR;
			System.err.println(String.format("time cost: %f second.", timecost));
			totalTime += timecost;
		}
		
		System.out.println(String.format("Average time cost: %f second.", totalTime / 5));

//		output(result);
	}

	public void output(Rectangular rec) {
		System.out.println("The rectangular is:");
		for (int i = 0; i < rec.row; i++) {
			for (int j = 0; j < rec.col; j++) {
				System.out.print(rec.value[i][j] + "\t");
			}
			System.out.println();
		}
	}

	public <E> Rectangular<E> rectangularMultiple(Type tp, Rectangular<E> item1, Rectangular<E> item2)
			throws Exception {
		if (item1.col != item2.row) {
			throw new Exception("Two rectangular can not multiply when A.column not equals to B.row");
		}

		Rectangular result = new Rectangular(item1.row, item2.col);
		boolean isDouble = tp.toString().equals("double");

		for (int i = 0; i < result.row; i++) {
			for (int j = 0; j < result.col; j++) {
				if (isDouble) {
					double value = 0;
					for (int k = 0; k < item1.col; k++) {
						value += ((Double) item1.value[i][k]) * ((Double) item2.value[k][j]);
					}
					result.value[i][j] = value;
				} else {
					int value = 0;
					for (int k = 0; k < item1.col; k++) {
						value += ((Integer) item1.value[i][k]) * ((Integer) item2.value[k][j]);
					}
					result.value[i][j] = value;
				}
			}
		}
		return result;
	}

	public Rectangular rectangularGenerator(Type tp, int length, int width) {
		Rectangular rec;
		Random rd = new Random();
		boolean isDouble = tp.toString().equals("double");
		int i = 0, j = 0;

		rec = new Rectangular(length, width);
		while (i < length) {
			while (j < width) {
				if (isDouble) {
					rec.value[i][j] = rd.nextDouble() * Double.MAX_VALUE;
				} else {
					rec.value[i][j] = rd.nextInt();
				}
				j++;
			}
			i++;
			j = 0;
		}

		return rec;
	}

	class Rectangular<E> {
		int row;
		int col;
		E[][] value;

		public Rectangular(int row, int column) {
			this.row = row;
			this.col = column;
			this.value = (E[][]) new Object[row][column];
		}
	}
}
