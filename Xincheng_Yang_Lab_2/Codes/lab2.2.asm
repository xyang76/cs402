.data 0x10010000
var1: .word 0x55 # var1 is a word (32 bit) with the initial value 0x55
var2: .word 0xaa # var2 is a word (32 bit) with the initial value 0xaa
var3: .word 0x33 # var3 is a word (32 bit) with the initial value 0x33
var4: .word 0xcc # var4 is a word (32 bit) with the initial value 0xcc
first: .byte 'X' # Xincheng
last: .byte 'Y'	 # Yang
.text
.globl main
main: addu $s0, $ra, $0
lw $t0, var1	# load var1
lw $t1, var4	# load var4
sw $t0, var4	# save var4
sw $t1, var1	# save var1
lw $t0, var2	# load var2
lw $t1, var3	# load var3
sw $t0, var3	# save var3
sw $t1, var2	# save var2
addu $ra, $0, $s0 # return address back in $31
jr $ra # return from main