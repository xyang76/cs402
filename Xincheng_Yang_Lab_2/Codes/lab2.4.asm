.data 0x10010000
var1: .word 0x20 # var1 is a word (32 bit) with the initial value 0x20
var2: .word 0x35 # var2 is a word (32 bit) with the initial value 0x25
.extern ext1, 4
.extern ext2, 4
.text
.globl main
main: addu $s0, $ra, $0
lw $t0, var1	#load var1 to t0
lw $t1, var2 	#load var2 to t0
sw $t0, ext2	#save var1 to ext2
sw $t1, ext1	#save var2 to ext1
addu $ra, $0, $s0 # return address back in $31
jr $ra # return from main