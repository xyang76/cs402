.data 0x10010000
var1: .word 0x55 # var1 is a word (32 bit) with the initial value 0x55
var2: .word 0xaa # var2 is a word (32 bit) with the initial value 0xaa
var3: .word 0x33 # var3 is a word (32 bit) with the initial value 0x33
var4: .word 0xcc # var4 is a word (32 bit) with the initial value 0xcc
first: .byte 'X' # Xincheng
last: .byte 'Y'	 # Yang
.text
.globl main
main: addu $s0, $ra, $0
lui $at, 4097		#load address

lw $t0, 0($at)		#lw $t0, var1

lw $t1, 12($at)		#lw $t1, var4

sw $t0, 12($at)		#save var4 to var1 address

sw $t1, 0($at)		#save var1 to var4 address

lw $t0, 4($at)		#lw $t0, var2

lw $t1, 8($at)		#lw $t1, var3

sw $t0, 8($at)		#save var3 to var2 address

sw $t1, 4($at)		#save var2 to var3 address
addu $ra, $0, $s0 # return address back in $31
jr $ra # return from main